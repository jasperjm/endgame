using System;
using System.Collections.Generic;

namespace Ants {
  public enum MissionType { Food, EnemyHill, UnseenLocation }

  public class Mission : IEquatable<Mission> {
    
    public MissionType Type { get; private set; }
    public Location Ant { get; private set; }
    public Route Route { get; private set; }
    public int Age { get; private set; }

    public Mission (MissionType type, Location ant, Location target, IGameState state) : this(type, ant, new Route(ant, target, state)) {
      
    }

    public Mission (MissionType type, Location ant, Route route) {
      this.Type = type;
      this.Ant = ant;
      this.Route = route;
    }

    public Location Peek() {
      return this.Route.Peek();
    }

    public Location Move() {
      this.Age++;

      Location loc = this.Route.Move();

      this.Ant = loc;

      return loc;
    }

    public bool Equals (Mission other) {
      if (other == null)
        return false;
      if (other == this)
        return true;

      return other.Ant == this.Ant && other.Route == this.Route;
    }

    public static bool operator == (Mission route, Mission other)
    {
        if (ReferenceEquals (null, route) && ReferenceEquals (null, other))
            return true;

        if (ReferenceEquals (null, route) || ReferenceEquals (null, other))
            return false;

        return other.Ant == route.Ant && other.Route == route.Route;
    }

    public static bool operator != (Mission route, Mission other)
    {
        return !(route == other);
    }

    public override int GetHashCode()
    {
      unchecked {
        return this.Ant.GetHashCode() * GameState.MaxMapSize * GameState.MaxMapSize + this.Route.GetHashCode();
      }
    }

    public override string ToString()
    {
      return "Ant " + this.Ant.ToString() + " going on " + this.Route.ToString() + " (age " + this.Age + ")";
    }
  }
}

