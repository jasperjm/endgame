using System.Collections.Generic;
using System;
using System.Linq;
using System.IO;

namespace Ants
{
    public class aStar
    {
        IGameState state;
        Location begin;
        Location target;
        int MaxSteps;
        public Stack<Location> stepStack = new Stack<Location>();
        Predicate<Location> CanMovePred;

        public aStar(Location begin, Location target, IGameState state) : this(begin, target, state, int.MaxValue, null) {

        }

        public aStar(Location begin, Location target, IGameState state, int maxSteps) : this(begin, target, state, maxSteps, null) {

        }

        public aStar(Location begin, Location target, IGameState state, int maxSteps, Predicate<Location> canMovePred)
        {
            this.state = state;
            this.begin = begin;
            this.target = target;
            this.CanMovePred = canMovePred;
            this.MaxSteps = maxSteps;

            //System.Console.WriteLine("Begin: " + begin);
            //System.Console.WriteLine("Target: " + target);

            FindRoute();
        }

        /// <summary>
        /// Creates the stepStack, query next step with nextStep()
        /// </summary>
        /// <param name="begin">Begin.</param>
        /// <param name="target">Target.</param>
        private void FindRoute()
        {
            if (this.state.TimeRemaining < 50) return;

            HashSet<Node> openSet = new HashSet<Node>();
            HashSet<Node> closedSet = new HashSet<Node>();

            Node root = new Node(begin);
            Node currentNode = root;
            openSet.Add(root);

            bool found = false;

            while (openSet.Count > 0 && !found)
            {
                currentNode = openSet.Min();
                //System.Console.WriteLine("Current: " + currentNode.location);
                
                if (currentNode.location == target || currentNode.stepsTaken > this.MaxSteps || this.state.TimeRemaining < 50)
                {
                    found = true;
                    break;
                }

                closedSet.Add(currentNode);
                openSet.Remove(currentNode);


                foreach (Node n in _getNeighbours(currentNode))
                {
                    if (this.state.TimeRemaining < 50) break;

                    //System.Console.WriteLine("Neighbour: " + n.location);

                    if (n.location == target)
                    {
                        //System.Console.WriteLine("Found");
                        currentNode = n;
                        found = true;
                        break;
                    }

                    if (this.CanMovePred != null && !this.CanMovePred(n.location))
                        continue;

                    Node sameNode = closedSet.FirstOrDefault(node => node == n);
                    if (sameNode != null && n.stepsTaken >= sameNode.stepsTaken)
                        continue;

                    sameNode = openSet.FirstOrDefault(node => node == n);

                    if (sameNode != null && n.stepsTaken < sameNode.stepsTaken) {
                        sameNode.stepsTaken = n.stepsTaken;
                        sameNode.parent = n.parent;
                    }

                    if (sameNode == null)
                        openSet.Add(n);
                }
            }
            //Unicorns and rainbows :) Route is found! Now fill the stack!
            if (found)
                _reconstructPath(root, currentNode);
        }

        private void _reconstructPath(Node root, Node currentNode)
        {
            Node node = currentNode;
            while (node != root)
            {
                stepStack.Push(node.location);

                node = node.parent;
            }
        }

        private Node[] _getNeighbours(Node currentNode)
        {
            Node[] neighbours = new Node[4];

            int i = 0;
            foreach (Location location in currentNode.location.GetNeighbours(state)) {
                neighbours[i] = new Node(location, currentNode);
                i++;
            }
                
            return neighbours;
        }

        private class Node : IEquatable<Node>, IComparable<Node>
        {
            public Location location;
            public Node parent;
            public int stepsTaken = 0;

            /// <summary>
            /// Initializes the tree, this constructor should only be used for the first node.
            /// </summary>
            /// <param name="location">Location of node.</param>
            public Node(Location location)
            {
                this.location = location;
            }

            public Node(Location location, Node parent)
            {
                this.location = location;
                this.parent = parent;
                this.stepsTaken = this.parent.stepsTaken + 1;
            }

            public override bool Equals (object obj) {
                if (ReferenceEquals (null, obj))
                    return false;
                if (ReferenceEquals (this, obj))
                    return true;
                if (obj.GetType() != typeof (Node))
                    return false;

                return Equals ((Node) obj);
            }

            public bool Equals (Node other) {
                if (ReferenceEquals (null, other))
                    return false;
                if (ReferenceEquals (this, other))
                    return true;

                return other.location == this.location;
            }

            public static bool operator == (Node node, Node other)
            {
                if (ReferenceEquals (null, node) && ReferenceEquals (null, other))
                    return true;

                if (ReferenceEquals (null, node) || ReferenceEquals (null, other))
                    return false;

                return node.location == other.location;
            }

            public static bool operator != (Node node, Node other)
            {
                return !(node == other);
            }

            public int CompareTo(Node other) {
                if (ReferenceEquals (null, other))
                    return 1;
                if (ReferenceEquals (this, other))
                    return 0;

                return this.stepsTaken - other.stepsTaken;
            }

            public override int GetHashCode()
            {
              unchecked {
                return this.location.GetHashCode();
              }
            }
        }
    }
}
