using System;
using System.Collections.Generic;

namespace Ants {

  public class Route : IEquatable<Route>, IComparable<Route> {

    public Location Start { get; private set; }
    public Location End { get; private set; }
    public IGameState State { get; private set; }
    public Stack<Location> Steps { get; private set; }
    public int Distance { 
      get {
        //return this.State.GetDistance(this.Start, this.End);
        if (this.Steps == null)
          return int.MaxValue;
        else
          return this.Steps.Count;
      } 
    }

    public Route (Location start, Location end, IGameState state) : this(start, end, state, int.MaxValue, null) {

    }

    public Route (Location start, Location end, IGameState state, int maxSteps) : this(start, end, state, maxSteps, null) {

    }

    public Route (Location start, Location end, IGameState state, int maxSteps, Predicate<Location> canMovePred) {
      this.Start = start;
      this.End = end;
      this.State = state;

      this.Steps = new aStar(start, end, state, maxSteps, canMovePred).stepStack;
    }

    public Location Peek() {
      if (this.Steps == null)
        return null;

      return this.Steps.Peek();
    }

    public Location Move() {
      //return this.End;
      if (this.Steps == null || this.Steps.Count == 0)
        return null;

      Location loc = this.Steps.Pop();

      this.Start = loc;

      return loc;
    }

    public bool Equals (Route other) {
      if (other == null)
        return false;
      if (other == this)
        return true;

      return other.Start == this.Start && other.End == this.End;
    }

    public static bool operator == (Route route, Route other)
    {
        if (ReferenceEquals (null, route) && ReferenceEquals (null, other))
            return true;

        if (ReferenceEquals (null, route) || ReferenceEquals (null, other))
            return false;

        return other.Start == route.Start && other.End == route.End;
    }

    public static bool operator != (Route route, Route other)
    {
        return !(route == other);
    }

    public int CompareTo(Route other) {
      if (other == null)
        return 1;
      if (other == this)
        return 0;

      return this.Distance - other.Distance;
    }

    public override int GetHashCode()
    {
      unchecked {
        return this.Start.GetHashCode() * GameState.MaxMapSize * GameState.MaxMapSize + this.End.GetHashCode();
      }
    }

    public override string ToString()
    {
      return "From " + this.Start.ToString() + " to " + this.End.ToString();
    }
  }
}

