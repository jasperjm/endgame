using System;
namespace Ants {
	public enum TileType { Ant, Dead, Land, Food, Water, Unseen, Hill }

  public class Tile {

    public TileType Type { get; private set; }
    public Location Location { get; private set; }

    public Tile(TileType type, Location location) {
      this.Type = type;
      this.Location = location;
    }
  }
}
