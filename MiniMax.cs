using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Ants {

  public class MiniMax {
    private List<Ant> myAnts;
    private List<Ant> enemyAnts;
    private IGameState state;
    private int bestValue = int.MinValue;
    private Node<Move> bestMove;
    private Node<Move> tree;
    private bool beAgressive = false;
    //private FileStream fs;
    //private StreamWriter sw;

    public MiniMax (HashSet<Ant> myAnts, HashSet<Ant> enemyAnts, IGameState state) {
      //fs = new FileStream("minimaxlog" + state.Turn + "x" + (state.TimeRemaining) + ".log", System.IO.FileMode.Create, System.IO.FileAccess.Write);
      //sw = new StreamWriter(fs);
      //sw.AutoFlush = true;

      this.myAnts = myAnts.ToList();
      this.enemyAnts = enemyAnts.ToList();
      this.state = state;

      //sw.WriteLine("Start MinMax");
      tree = new Node<Move> (null);
      max (0, tree, (Tile[,])state.Map.Clone());
    }

    private void max(int antIndex, Node<Move> currentMove, Tile[,] board) {
      if (antIndex < myAnts.Count) {
        //sw.WriteLine("Next move - " + antIndex);
        Ant myAnt = myAnts [antIndex];
        foreach (Location l in getLocations (myAnt, board)) {
          if (this.state.TimeRemaining < 2 * this.state.TurnTime / 3) break;

          Move move = new Move (myAnt, l);
          Node<Move> newMove = new Node<Move> (move);
          currentMove.Add (newMove);
          max (antIndex + 1, newMove, simulateMove(move, board));
        }
      } else {
        //sw.WriteLine("Max done, start Min");
        int value = min (0, currentMove, board);

        //sw.WriteLine("Value: " + value);
        //sw.WriteLine("Min done, finished");
        if (value > bestValue) {
          bestValue = value;
          bestMove = currentMove;
          //sw.WriteLine("Best value: " + bestValue);
        }
      }
    }

    private Tile[,] simulateMove(Move move, Tile[,] board) {
      // TODO: reweritre
      Tile[,] output = (Tile[,])board.Clone ();
      Tile old = output [move.Ant.Row, move.Ant.Col];

      output [move.Ant.Row, move.Ant.Col] = new Tile(TileType.Land, move.Ant);
      output [move.Destination.Row, move.Destination.Col] = new Tile(old.Type, move.Destination);

      return output;
    }

    private int min(int antIndex, Node<Move> currentMove, Tile[,] board) {
      if (antIndex < enemyAnts.Count) {
        int minValue = int.MaxValue;
        Ant enemyAnt = enemyAnts [antIndex];

        foreach (Location l in getLocations (enemyAnt, board)) {
          if (this.state.TimeRemaining < 2 * this.state.TurnTime / 3) break;

          Move move = new Move (enemyAnt, l);
          Node<Move> newMove = new Node<Move> (move);
          currentMove.Add (newMove);
          int value = min (antIndex + 1, newMove, simulateMove(move, board));
          if (value < bestValue)
            return int.MinValue;
          if (value < minValue) {
            minValue = value;
          }
        }
        return minValue;
      } else {
        int eval = evaluate(currentMove);
        //sw.WriteLine("Score: " + eval);
        return eval;
      }
    }

    private int evaluate(Node<Move> lastMove) {
      //sw.WriteLine("Evaluating");
      List<Ant> _myAnts = new List<Ant>();
      List<Ant> _enemyAnts = new List<Ant>();

      HashSet<Ant> deadAnts = new HashSet<Ant>();
      Dictionary<Ant, int> weakness = new Dictionary<Ant, int>();

      List<Move> moves = new List<Move> ();
      Node<Move> cycle = lastMove;
      while (cycle != null && cycle.Data != null) {
        //sw.WriteLine("Move " + cycle.Data.Ant + " to " + cycle.Data.Destination);
        moves.Add (cycle.Data);
        cycle = cycle.Parent;
      }
      moves.Reverse ();


      //sw.WriteLine("Simulate moves");
      foreach (Move move in moves) {
        foreach (Ant loc in myAnts) {
          if (move.Ant == loc) {
            Ant newAnt = new Ant(move.Destination.Row, move.Destination.Col, move.Ant.Team);
            _myAnts.Add (newAnt);
          }
        }
        foreach (Ant loc in enemyAnts) {
          if (move.Ant == loc) {
            Ant newAnt = new Ant(move.Destination.Row, move.Destination.Col, move.Ant.Team);
            _enemyAnts.Add (newAnt);
          }
        }
      }

      //sw.WriteLine("Find death score");
      int myDeadCount = 0;
      int enemyDeadCount = 0;
      foreach (Ant enemy in _enemyAnts) {
        foreach (Ant myAnt in _myAnts) {
          if (isAlphaDist(enemy, myAnt)) {
            //sw.WriteLine("Within alpha dist: " + enemy + " and " + myAnt);
            if (!weakness.ContainsKey(enemy))
              weakness[enemy] = 0;
            weakness[enemy]++;

            if (!weakness.ContainsKey(myAnt))
              weakness[myAnt] = 0;
            weakness[myAnt]++;
          }
        }
      }

      foreach (Ant myAnt in _myAnts) {
        if (this.state.TimeRemaining < 2 * this.state.TurnTime / 3) break;

        if (weakness.ContainsKey(myAnt) && weakness[myAnt] != 0) {
          foreach (Ant enemy in _enemyAnts) {
            if ((weakness.ContainsKey(enemy) && weakness[enemy] == 0) || !isAlphaDist(myAnt, enemy)) continue;

            if (!deadAnts.Contains(enemy) && weakness.ContainsKey(enemy) && weakness[enemy] >= weakness[myAnt]) {
              //sw.WriteLine("Enemy dies " + enemy);
              // enemy ant dies
              deadAnts.Add(enemy);
              enemyDeadCount++;
            }
            if (!deadAnts.Contains(myAnt) && weakness[myAnt] >= weakness[enemy]) {
              //sw.WriteLine("Friend dies " + myAnt);
              deadAnts.Add(myAnt);
              myDeadCount++;
            }
          }
          if (deadAnts.Contains(myAnt))
              deadAnts.Remove(myAnt);
          weakness[myAnt] = 0;
        }
      }
      //sw.WriteLine("enemyDeadCount: " + enemyDeadCount);
      //sw.WriteLine("myDeadCount: " + myDeadCount);
      if (beAgressive) return enemyDeadCount * 300 - myDeadCount * 180;
      else return enemyDeadCount * 512 - myDeadCount * (512+256) ;
    }

    private bool isAlphaDist(Location loc1, Location loc2) {
      int d_row = Math.Abs(loc1.Row - loc2.Row);
      d_row = Math.Min(d_row, this.state.Height - d_row);
      
      int d_col = Math.Abs(loc1.Col - loc2.Col);
      d_col = Math.Min(d_col, this.state.Width - d_col);

      int square = d_row * d_row + d_col * d_col;
      return (square <= this.state.AttackRadius2);
    }

    private List<Location> getLocations (Location ant, Tile[,] board) {
      List<Location> locs = new List<Location>();

      foreach (Direction direction in Ants.Aim.Keys) {
        Location loc = this.state.GetDestination(ant, direction);
        if (board[loc.Row, loc.Col].Type == TileType.Land)
          locs.Add(loc);
      }
      locs.Add(ant);

      return locs;
    }

    public List<Move> bestActions () {
      List<Move> ourMoves = new List<Move>();

      //sw.WriteLine("find best action");
      List<Move> moves = new List<Move> ();
      Node<Move> cycle = bestMove;
      while (cycle != null && cycle.Data != null) {
        moves.Add (cycle.Data);
        cycle = cycle.Parent;
      }
      moves.Reverse ();

      foreach (Move move in moves) {
        if (move.Ant.Team == 0) {
          ourMoves.Add(move);
        }
      }

      //sw.WriteLine("Best action complete");

      //sw.Close();
      //fs.Close();

      return ourMoves;
    }
  }

  public class Move {
    public Ant Ant { get; private set; }

    public Location Destination { get; private set; }

    public Move (Ant ant, Location destination) {
      this.Ant = ant;
      this.Destination = destination;
    }

  }


  public class Node<T> {
    private Node<T> _parent;
    private List<Node<T>> _children;
    private T _data;

    public IEnumerable<Node<T>> Children
    {
      get { return _children; }
    }

    public Node<T> Parent
    {
      get{return _parent;}
    }

    public T Data
    {
      get{return _data;}
    }

    public Node(T data)
    {
      _data = data;
      _children = new List<Node<T>>();
    }

    public void Add(Node<T> child)
    {
      _children.Add(child);
      child.SetParent(this);
    }

    public void Remove(Node<T> child)
    {
      child.SetParent(null);
      _children.Remove(child);
    }

    private void SetParent(Node<T> parent)
    {
      _parent = parent;
    }

    public void PrintChildren() {
      foreach (Node<T> n in _children) {
        Console.WriteLine (n._data);
        n.PrintChildren ();
      }
    }
  }
}