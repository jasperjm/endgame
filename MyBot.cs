using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ants {

	class MyBot : Bot {

		int[,] seenLocations;
		List<AntHill> enemyHills = new List<AntHill>();
		List<AntHill> razedHills = new List<AntHill>();

		IGameState currentState;
		Dictionary<Location, Location> orders = new Dictionary<Location, Location>();
		Dictionary<Location, Mission> missions = new Dictionary<Location, Mission>();
		Dictionary<Location, Mission> currentMissions;

		//StreamWriter sw;

		private bool DoMoveLocation (Location ant, Location destLoc) {
			if (this.currentState.TimeRemaining < 50) return false;

			if (orders.ContainsValue(ant)) return false;

			if (destLoc == ant && !this.orders.ContainsKey(destLoc))
			{
				orders.Add(destLoc, ant);
				return true;
			}

			ICollection<Direction> directions = this.currentState.GetDirections(ant, destLoc);
			foreach (Direction direction in directions) {
				if (this.currentState.TimeRemaining < 50) return false;

				if (DoMoveDirection(ant, direction)) {
					return true;
				}
			}
			return false;
		}

		private bool DoMoveDirection (Location ant, Direction direction) {
			if (this.currentState.TimeRemaining < 50) return false;

			if (orders.ContainsValue(ant)) return false;

			Location newLoc = this.currentState.GetDestination(ant, direction);
			if (AntCanMoveTo(newLoc)) {
				IssueOrder(ant, direction);
				orders.Add(newLoc, ant);
				return true;
			}
			else {
				return false;
			}
		}

		public override void DoTurn (IGameState state) {
			//FileStream fs = new FileStream("log" + state.Turn + ".log", System.IO.FileMode.Create, System.IO.FileAccess.Write);
			//sw = new StreamWriter(fs);
			//sw.AutoFlush = true;

			this.currentState = state;
			this.currentMissions = new Dictionary<Location, Mission>(this.missions);

			this.orders.Clear();
			this.missions.Clear();

			int t = this.currentState.TimeRemaining;
			UpdateSeenLocations();
			//sw.WriteLine("Update seen locations: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;

			t = this.currentState.TimeRemaining;
			UpdateEnemyHills();
			//sw.WriteLine("Add enemy hills: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;

			t = this.currentState.TimeRemaining;
			DontStepOnOwnHills();
			//sw.WriteLine("Don't step on own hills: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;


			t = this.currentState.TimeRemaining;
			Fight();
			//sw.WriteLine("Fight: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;


			t = this.currentState.TimeRemaining;
			ContinueMissions(MissionType.Food, mission => this.currentState[mission.Route.End].Type == TileType.Food);
			//sw.WriteLine("Continue food missions: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;
			
			t = this.currentState.TimeRemaining;
			MoveToFood();
			//sw.WriteLine("Move to food: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;
			
			t = this.currentState.TimeRemaining;
			ContinueMissions(MissionType.EnemyHill, mission => this.currentState[mission.Route.End].Type == TileType.Hill);
			//sw.WriteLine("Continue enemy hill missions: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;
			
			t = this.currentState.TimeRemaining;
			AttackEnemyHills();
			//sw.WriteLine("Attack enemy hills: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;
			

			t = this.currentState.TimeRemaining;
			ContinueMissions(MissionType.UnseenLocation);
			//sw.WriteLine("Continue unseen missions: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;
			
			t = this.currentState.TimeRemaining;
			ExploreUnseenAreas();
			//sw.WriteLine("Explore unseen areas: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;
			
			t = this.currentState.TimeRemaining;
			MoveAwayFromOwnHills();
			//sw.WriteLine("Move away from own hills: "+ (t - this.currentState.TimeRemaining));

			if (this.currentState.TimeRemaining < 50) return;

			//sw.Close();
			//fs.Close();
		}

		private void UpdateSeenLocations()
		{
			if (seenLocations == null) {
				seenLocations = new int[this.currentState.Height, this.currentState.Width];
			}

			//sw.WriteLine("Update last-seen turn for all locations");
			// Update last-seen turn for all locations
			foreach (Location loc in this.currentState.GetVisible())
			{
				seenLocations[loc.Row, loc.Col] = this.currentState.Turn;
			}
		}

		private void UpdateEnemyHills()
		{
			foreach (AntHill hill in enemyHills)
			{
				Tile tile = this.currentState[hill];
				if (tile.Type == TileType.Ant && ((Ant)tile.Location).Team != hill.Team)
					razedHills.Add(hill);
			}

			enemyHills.RemoveAll(hill => razedHills.Contains(hill));

			foreach (AntHill hill in this.currentState.EnemyHills) {
				if (!enemyHills.Contains(hill) && !razedHills.Contains(hill))
					enemyHills.Add(hill);
			}
		}

		private void DontStepOnOwnHills()
		{
			//sw.WriteLine("Prevent stepping on our own hills");
			// Prevent stepping on our own hills

			foreach (AntHill hill in this.currentState.MyHills) {
				orders.Add(new Location(hill.Row, hill.Col), null);
			}
		}

		private void Fight() {
			//sw.WriteLine("Fight");

			HashSet<Ant> fightingAnts = new HashSet<Ant>();

			foreach (Ant ant in this.currentState.MyAnts)
			{
				if (this.currentState.TimeRemaining < 2 * this.currentState.TurnTime / 3) return;

				if (fightingAnts.Contains(ant)) continue;

				Ant enemy = this.currentState.EnemyAnts.Find(other => this.currentState.GetDistance(ant, other) <= 8);
				if (enemy == null)
					continue;
				
				HashSet<Ant> friends = GetNearbyFriends(ant, this.currentState.MyAnts);
				foreach (Ant friend in friends) {
					fightingAnts.Add(friend);
				}

				if (this.currentState.TimeRemaining < 2 * this.currentState.TurnTime / 3) return;

				if (friends.Count == 1)
				{
					ICollection<Direction> dirs = this.currentState.GetDirections(ant, enemy);

					//sw.WriteLine("Attempting move by ant " + ant + " away from enemy " + enemy);
					foreach (Direction dir in dirs) {
						if (this.currentState.TimeRemaining < 2 * this.currentState.TurnTime / 3) return;

						Direction oppositeDir;
						switch (dir) {
							case Direction.North:
								oppositeDir = Direction.South;
								break;
							case Direction.South:
								oppositeDir = Direction.North;
								break;
							case Direction.East:
								oppositeDir = Direction.West;
								break;
							default:
								oppositeDir = Direction.East;
								break;
						}

						if (DoMoveDirection(ant, oppositeDir)) {
							//sw.WriteLine("Moved");
							break;
						}
					}
					continue;
				}

				HashSet<Ant> enemies = GetNearbyFriends(enemy, this.currentState.EnemyAnts);

				if (this.currentState.TimeRemaining < 2 * this.currentState.TurnTime / 3) return;

				//sw.WriteLine("Calculating best actions for friends of " + ant);
				int t = this.currentState.TimeRemaining;
				MiniMax mm = new MiniMax(friends, enemies, this.currentState);
				List<Move> moves = mm.bestActions();
				//sw.WriteLine("Duration: " + (t - this.currentState.TimeRemaining));

				if (this.currentState.TimeRemaining < 2 * this.currentState.TurnTime / 3) return;

				foreach (Move move in moves)
				{
					if (this.currentState.TimeRemaining < 2 * this.currentState.TurnTime / 3) return;

					Ant friend = move.Ant;
					Location loc = move.Destination;

					//sw.WriteLine("Attempting attack move by ant " + friend + " to loc " + loc);
					if (DoMoveLocation(friend, loc)) {
						//sw.WriteLine("Moved");
					}
				}
			}
		}

		private void ContinueMissions(MissionType type)
		{
			ContinueMissions(type, null);
		}

		private void ContinueMissions(MissionType type, Predicate<Mission> pred)
		{
			//sw.WriteLine("Follow up on missions");
			// Follow up on missions
			foreach (KeyValuePair<Location, Mission> pair in currentMissions) 
			{
				if (this.currentState.TimeRemaining < 50) return;

				Location ant = pair.Key;
				Mission mission = pair.Value;

				if (mission.Type != type)
					continue;

				if (this.currentState[mission.Ant].Type != TileType.Ant) 
					continue;

				if (orders.ContainsValue(ant)) 
					continue;

				if (mission.Age > 4)
					continue;

				if (pred != null && !pred(mission))
					continue;

				if (!AntCanMove(ant) && !this.orders.ContainsKey(ant)) 
				{
					orders.Add(ant, ant);
					continue;
				}

				Location nextLoc = mission.Move();
				if (nextLoc == null)
					continue;

				//sw.WriteLine("Attempting mission move by ant " + ant + " to loc " + mission.Route.End + " through " + nextLoc);
				if (DoMoveLocation(ant, nextLoc)) {
					//sw.WriteLine("Moved");
					missions[nextLoc] = mission;
				}
			}
		}

		private void MoveToFood()
		{
			//sw.WriteLine("Find routes to food");
			// Find routes to food

			List<Ant> availableAnts = new List<Ant>();
			foreach (Ant ant in this.currentState.MyAnts)
			{
				if (!orders.ContainsValue(ant))
					availableAnts.Add(ant);
			}
			if (availableAnts.Count == 0) return;

			List<Location> pursuedFood = new List<Location>();

			foreach (KeyValuePair<Location, Mission> pair in missions) 
			{
				Mission mission = pair.Value;

				if (mission.Type == MissionType.Food)
					pursuedFood.Add(mission.Route.End);
			}

			int maxInfluence = (int)Math.Floor(Math.Sqrt(this.currentState.ViewRadius2)) * 2;

			Dictionary<Location, int[,]> foodInfluenceMaps = new Dictionary<Location, int[,]>();
			foreach (Location foodLoc in this.currentState.FoodTiles) {
				if (this.currentState.TimeRemaining < 50) return;

				if (pursuedFood.Contains(foodLoc)) continue;

				int t = this.currentState.TimeRemaining;
				int[,] influenceMap = GetInfluenceMap(foodLoc, maxInfluence);
				//sw.WriteLine("Calculating influence map for " + foodLoc + ": "+ (t - this.currentState.TimeRemaining));
				foodInfluenceMaps.Add(foodLoc, influenceMap);
			}

			List<AntAndFoodAndInfluence> antFoodInfluences = new List<AntAndFoodAndInfluence>();
			foreach (Ant ant in availableAnts) {
				if (this.currentState.TimeRemaining < 50) return;

				if (orders.ContainsValue(ant)) continue;

				if (!AntCanMove(ant) && !this.orders.ContainsKey(ant)) 
				{
					orders.Add(ant, ant);
					continue;
				}

				//sw.WriteLine("Checking influence maps for ant " + ant);

				foreach (KeyValuePair<Location, int[,]> pair in foodInfluenceMaps)
				{
					if (this.currentState.TimeRemaining < 50) return;

					Location foodLoc = pair.Key;
					int[,] influenceMap = pair.Value;

					int influence = influenceMap[ant.Row, ant.Col];
					//sw.WriteLine("Influence for ant " + ant + " to food " + foodLoc + ": " + influence);
					if (influence > 0) {
						antFoodInfluences.Add(new AntAndFoodAndInfluence(ant, foodLoc, influence));
					}
				}
			}

			if (this.currentState.TimeRemaining < 50) return;
			antFoodInfluences = antFoodInfluences.OrderBy(tuple => -tuple.Influence).ToList();

			foreach (AntAndFoodAndInfluence tuple in antFoodInfluences)
			{
				if (this.currentState.TimeRemaining < 50) return;

				Ant ant = tuple.Ant;
				Location foodLoc = tuple.Food;
				int influence = tuple.Influence;

				if (pursuedFood.Contains(foodLoc)) continue;
				if (orders.ContainsValue(ant)) continue;

				if (!AntCanMove(ant) && !this.orders.ContainsKey(ant)) 
				{
					orders.Add(ant, ant);
					continue;
				}

				//sw.WriteLine("Calculating route from ant " + ant + " to food " + foodLoc);
				int t = this.currentState.TimeRemaining;
				Route route = this.BuildRoute(ant, foodLoc);
				//sw.WriteLine("Duration: "+ (t - this.currentState.TimeRemaining));
				Location nextLoc = route.Move();
				if (nextLoc == null) continue;

				if (this.currentState.TimeRemaining < 50) return;
				
				//sw.WriteLine("Attempting move by ant " + ant + " to food " + foodLoc + " through " + nextLoc);
				if (DoMoveLocation(ant, nextLoc)) {
					//sw.WriteLine("Moved");
					pursuedFood.Add(foodLoc);

					missions[nextLoc] = new Mission(MissionType.Food, nextLoc, route);
				}
			}
		}

		private void AttackEnemyHills()
		{
			//sw.WriteLine("Attack enemy hills");
			// Attack enemy hills

			List<Ant> availableAnts = new List<Ant>();
			foreach (Ant ant in this.currentState.MyAnts)
			{
				if (!orders.ContainsValue(ant))
					availableAnts.Add(ant);
			}
			if (availableAnts.Count == 0) return;

			int maxInfluence = (int)Math.Floor(Math.Sqrt(this.currentState.ViewRadius2)) * 4;

			Dictionary<Location, int[,]> hillInfluenceMaps = new Dictionary<Location, int[,]>();
			foreach (AntHill hill in enemyHills) {
				if (this.currentState.TimeRemaining < 50) return;

				int t = this.currentState.TimeRemaining;
				int[,] influenceMap = GetInfluenceMap(hill, maxInfluence);
				//sw.WriteLine("Calculating influence map for " + hill + ": "+ (t - this.currentState.TimeRemaining));
				hillInfluenceMaps.Add(hill, influenceMap);
			}

			foreach (Ant ant in availableAnts) {
				if (this.currentState.TimeRemaining < 50) return;

				if (orders.ContainsValue(ant)) continue;

				if (!AntCanMove(ant) && !this.orders.ContainsKey(ant)) 
				{
					orders.Add(ant, ant);
					continue;
				}

				//sw.WriteLine("Checking influence maps for ant " + ant);

				Location hillLoc = null;
				int highestInfluence = 0;
				foreach (KeyValuePair<Location, int[,]> pair in hillInfluenceMaps)
				{
					if (this.currentState.TimeRemaining < 50) return;

					Location loc = pair.Key;
					int[,] influenceMap = pair.Value;

					int influence = influenceMap[ant.Row, ant.Col];
					//sw.WriteLine("Influence for ant " + ant + " to hill " + loc + ": " + influence);
					if (influence > highestInfluence) {
						hillLoc = loc;
						highestInfluence = influence;
					}
				}

				if (hillLoc == null) continue;

				if (this.currentState.TimeRemaining < 50) return;

				//sw.WriteLine("Calculating route from ant " + ant + " to hill " + hillLoc);
				int t = this.currentState.TimeRemaining;
				Route route = this.BuildRoute(ant, hillLoc);
				//sw.WriteLine("Duration: " + (t - this.currentState.TimeRemaining));
				Location nextLoc = route.Move();
				if (nextLoc == null) continue;

				if (this.currentState.TimeRemaining < 50) return;
				
				//sw.WriteLine("Attempting move by ant " + ant + " to food " + hillLoc + " through " + nextLoc);
				if (DoMoveLocation(ant, nextLoc)) {
					//sw.WriteLine("Moved");
					missions[nextLoc] = new Mission(MissionType.EnemyHill, nextLoc, route);
				}
			}
		}

		private void ExploreUnseenAreas()
		{
			//sw.WriteLine("Explore unseen areas");
			// Explore unseen areas

			List<Ant> availableAnts = new List<Ant>();
			foreach (Ant ant in this.currentState.MyAnts)
			{
				if (!orders.ContainsValue(ant))
					availableAnts.Add(ant);
			}
			if (availableAnts.Count == 0) return;

			List<Location> pursuedLocations = new List<Location>();
			foreach (KeyValuePair<Location, Mission> pair in missions) 
			{
				Mission mission = pair.Value;

				if (mission.Type == MissionType.UnseenLocation)
					pursuedLocations.Add(mission.Route.End);
			}
			
			if (this.currentState.TimeRemaining < 50) return;
			List<Location> invisibles = this.currentState.GetInvisibleEdges().OrderBy(loc => seenLocations[loc.Row, loc.Col] - this.currentState.Turn).ToList();
			
			foreach (Ant ant in availableAnts) {
				if (this.currentState.TimeRemaining < 50) return;

				if (orders.ContainsValue(ant)) continue;

				if (!AntCanMove(ant) && !this.orders.ContainsKey(ant)) 
				{
					orders.Add(ant, ant);
					continue;
				}

				List<LocationAndDistance> invisibleDistances = new List<LocationAndDistance>();
				foreach (Location loc in invisibles)
				{
					if (this.currentState.TimeRemaining < 50) return;

					if (pursuedLocations.Contains(loc)) continue;
					if (!this.currentState.GetIsPassable(loc)) continue;

					int distance = this.currentState.GetDistance(ant, loc);
					invisibleDistances.Add(new LocationAndDistance(loc, distance));
				}

				if (this.currentState.TimeRemaining < 50) return;
				invisibleDistances = invisibleDistances.OrderBy(tuple => tuple.Distance).ToList();

				foreach (LocationAndDistance tuple in invisibleDistances)
				{
					if (this.currentState.TimeRemaining < 50) return;

					Location invisibleLoc = tuple.Location;
					int distance = tuple.Distance;

					//sw.WriteLine("Calculating route from ant " + ant + " to invisible " + invisibleLoc);
					int t = this.currentState.TimeRemaining;
					Route route = this.BuildRoute(ant, invisibleLoc, distance * 3);
					//sw.WriteLine("Duration: "+ (t - this.currentState.TimeRemaining));
					Location nextLoc = route.Move();
					if (nextLoc == null) continue;

					if (this.currentState.TimeRemaining < 50) return;

					//sw.WriteLine("Attempting move by ant " + ant + " to invisible " + invisibleLoc + " through " + nextLoc);
					if (DoMoveLocation(ant, nextLoc)) {
						//sw.WriteLine("Moved");
						pursuedLocations.Add(invisibleLoc);

						missions[nextLoc] = new Mission(MissionType.UnseenLocation, nextLoc, route);
						break;
					}
				}
			}
		}

		private void MoveAwayFromOwnHills()
		{
			//sw.WriteLine("Move away from hills");
			// Move away from hills
			foreach (AntHill hill in this.currentState.MyHills) {
				if (this.currentState.TimeRemaining < 50) return;

				Tile tile = this.currentState[(Location)hill];

				// If any of our ants is at a hill's location AND
				// if that ant hasn't been ordered away.
				if (tile.Type == TileType.Ant && 
						!orders.ContainsValue((Ant)tile.Location)) {

					//sw.WriteLine("Attempting move by ant " + tile.Location + " away from hill " + hill);
					foreach (Direction direction in Ants.Aim.Keys) {
						if (this.currentState.TimeRemaining < 50) return;

						if (DoMoveDirection((Ant)tile.Location, direction)) {
							//sw.WriteLine("Moved");
							break;
						}
					}
				}
			}
		}

		private bool AntCanMoveTo (Location location) {
			// We can't move here when...

			// the tile has water, or
			if (!this.currentState.GetIsPassable(location)) 
				return false;

			// someone is moving to the tile, or
			if (this.orders.ContainsKey(location)) 
				return false;
													
			// someone is currently at the tile, AND is not moving anywhere
			if (this.currentState.GetIsOccupied(location) && !this.orders.ContainsValue(location))
				return false;

      return true;
		}

		private bool AntCanMove (Location ant)
		{
			foreach	(Location location in ant.GetNeighbours(this.currentState))
			{
				if (AntCanMoveTo(location))
					return true;
			}

			return false;
		}

		private Route BuildRoute(Location start, Location end) {
			return BuildRoute(start, end, int.MaxValue);
		}

		private Route BuildRoute(Location start, Location end, int maxValue) {
			return new Route(start, end, this.currentState, maxValue, location => this.AntCanMoveTo(location));
		}

		private int[,] GetInfluenceMap(Location start)
		{
			return GetInfluenceMap(start, 10);
		}

		private int[,] GetInfluenceMap(Location start, int influence)
		{
	    int[,] map = new int[this.currentState.Height, this.currentState.Width];

	    HashSet<Location> openSet = new HashSet<Location>();
	    openSet.Add(start);

	    HashSet<Location> newNodes = new HashSet<Location>();
	    while (influence > 0 && openSet.Count > 0)
	    {
				if (this.currentState.TimeRemaining < 50) break;

        foreach (Location node in openSet) 
        {
					if (this.currentState.TimeRemaining < 50) break;

      		map[node.Row, node.Col] = influence;

          foreach (Location neighbour in node.GetNeighbours(this.currentState))
          {		
						if (this.currentState.TimeRemaining < 50) break;

        		if (this.seenLocations[neighbour.Row, neighbour.Col] == 0) continue;
        		if (!this.currentState.GetIsPassable(neighbour)) continue;

            if (map[neighbour.Row, neighbour.Col] == 0 && !newNodes.Contains(neighbour))
            	newNodes.Add(neighbour);
          }
        }
       
        openSet = new HashSet<Location>(newNodes);
        newNodes.Clear();
        influence--;
    	}

	    return map;
		}

    private HashSet<Ant> GetNearbyFriends(Ant ant, List<Ant> allAnts)
    {
      HashSet<Ant> friends = new HashSet<Ant>();

      List<Ant> openList = new List<Ant>();

      openList.Add(ant);
      while (openList.Count > 0 && friends.Count < 5)
      {
				if (this.currentState.TimeRemaining < 50) break;

        ant = openList.First();

        friends.Add(ant);

        foreach (Ant other in allAnts)
        {
					if (this.currentState.TimeRemaining < 50) break;

          if (friends.Contains(other) || openList.Contains(other)) continue;

          if (this.currentState.GetDistance(ant, other) <= 5)
          {
            openList.Add(other);
          }
        }

        openList.Remove(ant);
      }

      return friends;
    }
		
		public static void Main (string[] args) {
			new Ants().PlayGame(new MyBot());
		}
	}	

	class LocationAndDistance {
		public Location Location { get; private set; }
		public int Distance { get; private set; }

		public LocationAndDistance(Location location, int distance)
		{
			this.Location = location;
			this.Distance = distance;
		}
	}

	class AntAndFoodAndInfluence {
		public Ant Ant { get; private set; }
		public Location Food { get; private set; }
		public int Influence { get; private set; }

		public AntAndFoodAndInfluence(Ant ant, Location food, int influence)
		{
			this.Ant = ant;
			this.Food = food;
			this.Influence = influence;
		}
	}
}